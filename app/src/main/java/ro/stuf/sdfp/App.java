package ro.stuf.sdfp;

import android.content.Context;
import ro.stuf.sdfp.news.NewsManager;
import ro.stuf.sdfp.themes.ThemeManager;

public class App {
  public static Context context;

  public final static String EXTRA_ITEM_INDEX = "itemIndex";
  public final static NewsManager newsManager = new NewsManager();
  public final static ThemeManager themeManager = new ThemeManager();
}
