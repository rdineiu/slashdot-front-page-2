package ro.stuf.sdfp.activities;

import android.os.Bundle;
import ro.stuf.sdfp.R;
import ro.stuf.sdfp.ui.ThemesListView;

public class ThemesActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_themes);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final ThemesListView themesListView = getThemesListView();
        themesListView.setOnSelectListener(new ThemesListView.OnSelectListener() {
            @Override
            public void itemSelected() {
                finish();
            }
        });

        themesListView.show();
    }

    private ThemesListView getThemesListView() {
        return (ThemesListView) findViewById(R.id.themes_list_view);
    }
}
