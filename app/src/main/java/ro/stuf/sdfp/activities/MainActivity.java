package ro.stuf.sdfp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import ro.stuf.sdfp.App;
import ro.stuf.sdfp.R;
import ro.stuf.sdfp.news.NewsItem;
import ro.stuf.sdfp.news.NewsManager;
import ro.stuf.sdfp.ui.NewsListView;

import java.util.List;

public class MainActivity extends BaseActivity {
  private boolean isLoading = false;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    load();
  }

  @Override
  protected void onStart() {
    super.onStart();
    if (App.themeManager.isCurrentThemeChanged()) {
      applyTheme();
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.main_activity_actions, menu);
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.action_refresh:
        App.newsManager.clear();
        load();
        return true;
      case R.id.action_themes:
        final Intent intent = new Intent(this, ThemesActivity.class);
        startActivity(intent);
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  private void applyTheme() {
    System.exit(0);
  }

  private void load() {
    if (isLoading) {
      return;
    }
    isLoading = true;

    final View progressView = findViewById(R.id.news_list_progress_view);
    final View newsListView = findViewById(R.id.news_list_view);

    newsListView.setVisibility(View.GONE);
    progressView.setVisibility(View.VISIBLE);

    App.newsManager.getItems(new NewsManager.Callback() {
      @Override
      public void onItemsReceived(List<NewsItem> items) {
        progressView.setVisibility(View.GONE);
        if (items != null && !items.isEmpty()) {
          newsListView.setVisibility(View.VISIBLE);
          ((NewsListView) newsListView).show();
        }
        isLoading = false;
      }
    });
  }
}
