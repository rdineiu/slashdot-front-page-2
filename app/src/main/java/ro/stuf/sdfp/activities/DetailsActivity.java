package ro.stuf.sdfp.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import ro.stuf.sdfp.App;
import ro.stuf.sdfp.R;
import ro.stuf.sdfp.news.NewsItem;
import ro.stuf.sdfp.news.NewsPerson;
import ro.stuf.sdfp.news.StyleRange;

import java.util.List;

public class DetailsActivity extends BaseActivity {
  private NewsItem item;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_details);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    presentItem(getItem());
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.details_activity_actions, menu);

    final MenuItem shareItem = menu.findItem(R.id.action_share);
    final ShareActionProvider shareActionProvider =
        (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);

    final Intent intent = new Intent(Intent.ACTION_SEND);
    intent.setType("text/plain");
    intent.putExtra(Intent.EXTRA_TEXT, getShareText());
    shareActionProvider.setShareIntent(intent);

    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == R.id.action_go) {
      startActivity(new Intent(Intent.ACTION_VIEW, getItemUri()));
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  private String getShareText() {
    final NewsItem item = getItem();
    return item.getTitle() + ", " + item.getShortLink();
  }

  private NewsItem getItem() {
    if (item == null) {
      final int itemIndex = getIntent().getIntExtra(App.EXTRA_ITEM_INDEX, 0);
      item = App.newsManager.getCurrentItems().get(itemIndex);
    }
    return item;
  }

  private Uri getItemUri() {
    return Uri.parse(getItem().getLink());
  }

  private void presentItem(final NewsItem item) {
    presentTitle(item);
    presentText(item);
    presentDetails(item);
  }

  private void presentTitle(final NewsItem item) {
    final TextView titleTextView = (TextView) findViewById(R.id.news_item_title);
    titleTextView.setText(item.getTitle());
  }

  private void addDetails(final String key, final SpannableString value, final boolean isLink) {
    final TableLayout detailsTable = (TableLayout) findViewById(R.id.news_item_details);

    final TableRow row = (TableRow) getLayoutInflater().inflate(R.layout.activity_details_row, null);
    final TextView keyView = (TextView) row.findViewById(R.id.news_item_details_key);
    final TextView valueView = (TextView) row.findViewById(R.id.news_item_details_value);

    keyView.setText(key);
    valueView.setText(value);

    if (isLink) {
      prepareTextView(valueView);
    }

    detailsTable.addView(row);
  }

  private SpannableString toLink(final String text, final String url) {
    SpannableString link = new SpannableString(text);
    if (url.startsWith("http:") ||
        url.startsWith("https:") ||
        (url.startsWith("mailto:") && isMailClientAvailable())) {
      final URLSpan urlSpan = new URLSpan(url);
      link.setSpan(urlSpan, 0, text.length(), 0);
    }
    return link;
  }

  private void presentDetails(final NewsItem item) {
    final NewsPerson poster = item.getPoster();

    addDetails("Department", new SpannableString(item.getDepartment()), false);
    addDetails("Author", toLink(poster.handle, poster.link), true);
    addDetails("Comments", new SpannableString(item.getCommentCount().toString()), false);
  }

  private void presentText(final NewsItem item) {
    final SpannableString text = new SpannableString(item.getFullText());

    for (final StyleRange styleRange : item.getContentsStyle()) {
      if (styleRange.type == StyleRange.Type.LINK) {
        final URLSpan urlSpan = new URLSpan(styleRange.value);
        text.setSpan(urlSpan, styleRange.start, styleRange.end, 0);
      }
    }

    final TextView textView = (TextView) findViewById(R.id.news_item_text);
    prepareTextView(textView);
    textView.setText(text);
  }

  private void prepareTextView(final TextView textView) {
    textView.setMovementMethod(LinkMovementMethod.getInstance());
    textView.setSingleLine(false);
  }

  private boolean isMailClientAvailable() {
    Intent intent = new Intent(Intent.ACTION_SEND);
    intent.setType("text/html");
    final PackageManager packageManager = getApplicationContext().getPackageManager();
    List<ResolveInfo> list = packageManager.queryIntentActivities(intent, 0);
    return list.size() != 0;
  }
}
