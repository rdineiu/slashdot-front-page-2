package ro.stuf.sdfp.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import ro.stuf.sdfp.App;
import ro.stuf.sdfp.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BaseActivity extends ActionBarActivity {
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    setupApp();
    setTheme(App.themeManager.getCurrentTheme().resourceId);
    super.onCreate(savedInstanceState);
    CalligraphyConfig.initDefault("fonts/NotoSans-Regular.ttf", R.attr.fontPath);
  }

  @Override
  protected void attachBaseContext(Context newBase) {
    super.attachBaseContext(new CalligraphyContextWrapper(newBase));
  }

  private void setupApp() {
    App.context = this;
  }
}
