package ro.stuf.sdfp.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import ro.stuf.sdfp.App;
import ro.stuf.sdfp.R;
import ro.stuf.sdfp.themes.Theme;

public class ThemesListAdapter extends ArrayAdapter<Theme> {
  private static final int layoutId = R.layout.activity_themes_item;

  @Override
  public View getView(final int position, View view, final ViewGroup parent) {
    final Theme[] themes = App.themeManager.getThemes();
    final Theme theme = themes[position];

    view = createView(view, parent);

    ((TextView) view.findViewById(R.id.theme_title)).setText(theme.title);

    return view;
  }

  private View createView(final View view, final ViewGroup parent) {
    if (view == null) {
      return LayoutInflater.from(getContext()).inflate(layoutId, parent, false);
    } else {
      return view;
    }
  }

  public ThemesListAdapter(Context context) {
    super(context, layoutId, App.themeManager.getThemes());
  }
}
