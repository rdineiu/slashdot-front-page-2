package ro.stuf.sdfp.ui;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import ro.stuf.sdfp.App;
import ro.stuf.sdfp.activities.DetailsActivity;

public class NewsListView extends ListView {
  private static NewsListAdapter adapter;
  private static int lastItemPosition = 0;

  public NewsListView(final Context context, AttributeSet attrs) {
    super(context, attrs);

    setOnItemClickListener(new OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        lastItemPosition = position;
        final Intent intent = new Intent(context, DetailsActivity.class);
        intent.putExtra(App.EXTRA_ITEM_INDEX, position);
        context.startActivity(intent);
      }
    });
  }

  public void show() {
    setAdapter(getNewsListAdapter());
    setSelection(lastItemPosition);
  }

  private NewsListAdapter getNewsListAdapter() {
    if (adapter == null) {
      adapter = new NewsListAdapter(getContext(), App.newsManager.getCurrentItems());
    }
    return adapter;
  }
}
