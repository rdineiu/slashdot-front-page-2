package ro.stuf.sdfp.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import ro.stuf.sdfp.App;
import ro.stuf.sdfp.themes.Theme;
import ro.stuf.sdfp.themes.ThemeManager;

public class ThemesListView extends ListView {
  private static ThemesListAdapter adapter;

  public static interface OnSelectListener { public void itemSelected(); }
  private OnSelectListener onSelectListener;

  public ThemesListView(final Context context, AttributeSet attrs) {
    super(context, attrs);

    setOnItemClickListener(new OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final ThemeManager themeManager = App.themeManager;
        final Theme[] themes = themeManager.getThemes();
        themeManager.setCurrentTheme(themes[position]);

        if (onSelectListener != null) {
          onSelectListener.itemSelected();;
        }
      }
    });
  }

  public void show() {
    setAdapter(getThemesListAdapter());
  }

  private ThemesListAdapter getThemesListAdapter() {
    if (adapter == null) {
      adapter = new ThemesListAdapter(getContext());
    }
    return adapter;
  }

  public OnSelectListener getOnSelectListener() {
    return onSelectListener;
  }

  public void setOnSelectListener(OnSelectListener onSelectListener) {
    this.onSelectListener = onSelectListener;
  }
}
