package ro.stuf.sdfp.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import ro.stuf.sdfp.R;
import ro.stuf.sdfp.news.NewsItem;

import java.util.List;

public class NewsListAdapter extends ArrayAdapter<NewsItem> {
  private static final int layoutId = R.layout.activity_main_item;

  public NewsListAdapter(final Context context, final List<NewsItem> items) {
    super(context, layoutId, items);
  }

  @Override
  public View getView(final int position, View view, final ViewGroup parent) {
    final NewsItem item = getItem(position);

    view = createView(view, parent);

    ((TextView) view.findViewById(R.id.list_item_title)).setText(item.getTitle());
    ((TextView) view.findViewById(R.id.list_item_text)).setText(item.getAbbreviatedText());

    return view;
  }

  private View createView(final View view, final ViewGroup parent) {
    if (view == null) {
      return LayoutInflater.from(getContext()).inflate(layoutId, parent, false);
    } else {
      return view;
    }
  }
}
