package ro.stuf.sdfp.news;

import org.joda.time.DateTime;

import java.util.List;

public interface NewsItem {
  public String getTitle();
  public String getLink();
  public String getShortLink();
  public Integer getCommentCount();
  public NewsPerson getPoster();
  public NewsPerson getAuthor();
  public DateTime getTime();
  public String getDepartment();
  public String getFullText();
  public String getAbbreviatedText();
  public String getContentsHTML();
  public List<StyleRange> getContentsStyle();
}
