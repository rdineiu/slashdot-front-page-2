package ro.stuf.sdfp.news;

public class StyleRange {
  public static enum Type { QUOTE, LINK }

  public Type type;
  public int start, end;
  public String value;
}
