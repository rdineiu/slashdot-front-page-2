package ro.stuf.sdfp.news;

public class NewsPersonAnonymous extends NewsPerson {
  public NewsPersonAnonymous() {
    this.handle = "anonymous";
    this.link = null;
  }
}
