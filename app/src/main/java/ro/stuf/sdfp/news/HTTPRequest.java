package ro.stuf.sdfp.news;

import android.os.AsyncTask;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class HTTPRequest extends AsyncTask<String, String, String> {
  private Exception exception;
  private Callback callback;

  public static interface Callback {
    public void onSuccess(String result);
    public void onError(Exception exception);
  }

  public HTTPRequest(Callback callback) {
    this.callback = callback;
  }

  @Override
  protected String doInBackground(String... params) {
    final String url = params[0];
    final HttpClient client = new DefaultHttpClient();
    HttpResponse response;
    try {
      response = client.execute(new HttpGet(url));
      final StatusLine status = response.getStatusLine();
      if (status.getStatusCode() == HttpStatus.SC_OK) {
        final ByteArrayOutputStream output = new ByteArrayOutputStream();
        response.getEntity().writeTo(output);
        output.close();
        return output.toString();
      } else {
        response.getEntity().getContent().close();
        exception = new IOException(status.getReasonPhrase());
      }
    } catch (Exception exception) {
      this.exception = exception;
    }
    return null;
  }

  @Override
  protected void onPostExecute(String result) {
    super.onPostExecute(result);
    if (result == null) {
      callback.onError(exception);
    } else {
      callback.onSuccess(result);
    }
  }
}
