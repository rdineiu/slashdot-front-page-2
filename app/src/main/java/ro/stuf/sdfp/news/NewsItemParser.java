package ro.stuf.sdfp.news;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.List;

public class NewsItemParser {
  public List<NewsItem> parse(final String html) {
    final List<NewsItem> items = new ArrayList<NewsItem>();
    for (final Element article : Jsoup.parse(html).getElementsByTag("article")) {
      if (article.attr("data-fhtype").equals("story")) {
        final NewsItemHTML item = new NewsItemHTML(article);
        if (item.hasBody()) {
          items.add(item);
        }
      }
    }
    return items;
  }
}
