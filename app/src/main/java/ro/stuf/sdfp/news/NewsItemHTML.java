package ro.stuf.sdfp.news;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NewsItemHTML implements NewsItem {
  private static final DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("'on' EE MMM dd, yyyy '@'hh:mma");
  private static final Pattern departmentPattern = Pattern.compile("^.*from the (.+?) dept\\.$");
  private static final Pattern writesHTMLPattern = Pattern.compile("(^.+writes[^<]+?)<i>", Pattern.MULTILINE);
  private static final Pattern anonymousPattern = Pattern.compile("^[^<]*?anonymous[^<]+?writes");
  private static final Pattern newlinePattern = Pattern.compile("(?:\\s*</?(?:br|i)[^>]*>\\s*)+", Pattern.CASE_INSENSITIVE);
  private static final Pattern shortLinkPattern = Pattern.compile("http.*/", Pattern.CASE_INSENSITIVE);
  private static final Pattern iDevicePattern = Pattern.compile("\\bI(Phone|P[ao]d)");
  private static final String baseURL = "http://slashdot.org";

  private String title;
  private String link;
  private Integer commentCount;
  private NewsPerson poster;
  private NewsPerson author;
  private DateTime time;
  private String department;
  private String contentsText;
  private String contentsHTML;
  private List<StyleRange> contentsStyle;

  private final Element article;

  private Element header;
  private Element story;
  private Element storyLink;
  private Element details;
  private Element body;
  private Element bodyContents;
  private String text;

  public NewsItemHTML(final Element article) {
    this.article = article;
  }

  public String getTitle() {
    if (title == null) {
      title = getStoryLink().text();
      final Matcher matcher = iDevicePattern.matcher(title);
      if (matcher.find()) {
        title = matcher.replaceAll("i$1");
      }
    }
    return title;
  }

  public String getLink() {
    if (link == null) {
      link = "http:" + getStoryLink().attr("href");
    }
    return link;
  }

  @Override
  public String getShortLink() {
    final String link = getLink();
    final Matcher matcher = shortLinkPattern.matcher(link);
    if (matcher.find()) {
      final String shortLink = matcher.group(0);
      return shortLink.substring(0, shortLink.length() - 1);
    }
    return link;
  }

  public Integer getCommentCount() {
    if (commentCount == null) {
      commentCount = Integer.valueOf(
          getStory().getElementsByClass("comments").first().text()
      );
    }
    return commentCount;
  }

  public NewsPerson getPoster() {
    if (poster == null) {
      final Element posterLink = getDetails().getElementsByTag("a").first();
      poster = new NewsPerson();
      poster.handle = posterLink.text();
      poster.link = posterLink.attr("href");
    }
    return poster;
  }

  public NewsPerson getAuthor() {
    if (author == null) {
      if (getFullText().contains("writes")) {
        final Element authorLink =
            getBody().getElementsByTag("i").first().previousElementSibling();

        if (authorLink == null) {
          if (anonymousPattern.matcher(getContentsHTML()).find()) {
            author = new NewsPersonAnonymous();
          } else {
            author = getPoster();
          }
        } else {
          author = new NewsPerson();
          author.handle = authorLink.text();
          author.link = baseURL + authorLink.attr("href");
        }
      } else {
        author = getPoster();
      }
    }
    return author;
  }

  public DateTime getTime() {
    if (time == null) {
      time = DateTime.parse(
          getDetails().getElementsByTag("time").first().attr("datetime"),
          dateFormatter
      );
    }
    return time;
  }

  public String getDepartment() {
    if (department == null) {
      department = departmentPattern.matcher(getDetails().text()).replaceFirst("$1");
    }
    return department;
  }

  public String getFullText() {
    if (text == null) {
      final String br = "__##_BR_##__";
      text = Jsoup.parse(newlinePattern.matcher(getContentsHTML()).replaceAll(br)).text();
      text = text.replaceAll(br, "\n\n").trim();
    }
    return text;
  }

  public String getAbbreviatedText() {
    if (contentsText == null) {
      final String html = getContentsHTML();
      final String text = getFullText();

      final Matcher matcher = writesHTMLPattern.matcher(html);
      if (matcher.find()) {
        final String matched = Jsoup.parse(matcher.group(1)).text().trim();
        int index = text.indexOf(matched) + matched.length();
        contentsText = text.substring(index).trim();
      } else {
        contentsText = text;
      }

      contentsText = contentsText.replaceAll("\n+", " ");
    }
    return contentsText;
  }

  public String getContentsHTML() {
    if (contentsHTML == null) {
      final Element body = getBodyContents();
      int count = 0;
      for (final Element i : body.getElementsByTag("i")) {
        if (count++ > 0) {
          i.tagName("span");
        }
      }
      contentsHTML = body.html();
    }
    return contentsHTML;
  }

  public List<StyleRange> getContentsStyle() {
    if (contentsStyle == null) {
      contentsStyle = new ArrayList<StyleRange>();

      final String text = getFullText();

      for (final Element quoteTag : getBodyContents().getElementsByTag("i")) {
        final String quoteText = quoteTag.text();
        final StyleRange styleRange = new StyleRange();
        styleRange.type = StyleRange.Type.QUOTE;
        styleRange.start = text.indexOf(quoteText);
        if (styleRange.start > 0) {
          styleRange.end = styleRange.start + quoteText.length();
          contentsStyle.add(styleRange);
        }
      }

      for (final Element linkTag : getBodyContents().getElementsByTag("a")) {
        final String linkText = linkTag.text();
        final StyleRange styleRange = new StyleRange();
        styleRange.type = StyleRange.Type.LINK;
        styleRange.start = text.indexOf(linkText);
        if (styleRange.start > 0) {
          styleRange.end = styleRange.start + linkText.length();
          styleRange.value = linkTag.attr("href");
          if (styleRange.value.startsWith("//")) {
            styleRange.value = baseURL + styleRange.value.substring(1);
          } else if (styleRange.value.startsWith("/")) {
            styleRange.value = baseURL + styleRange.value;
          }
          contentsStyle.add(styleRange);
        }
      }
    }
    return contentsStyle;
  }

  public boolean hasBody() {
    return getBody() != null;
  }

  private Element getArticle() {
    return this.article;
  }

  private Element getHeader() {
    if (header == null) {
      header = getArticle().getElementsByTag("header").first();
    }
    return header;
  }

  private Element getStory() {
    if (story == null) {
      story = getHeader().getElementsByClass("story").first();
    }
    return story;
  }

  private Element getStoryLink() {
    if (storyLink == null) {
      storyLink = getStory().getElementsByTag("a").first();
    }
    return storyLink;
  }

  private Element getDetails() {
    if (details == null) {
      details = getHeader().getElementsByClass("details").first();
    }
    return details;
  }

  private Element getBody() {
    if (body == null) {
      body = getArticle().getElementsByClass("body").first();
    }
    return body;
  }

  private Element getBodyContents() {
    if (bodyContents == null) {
      bodyContents = getBody().children().first();
    }
    return bodyContents;
  }

  @Override
  public boolean equals(Object otherItem) {
    return otherItem instanceof NewsItemHTML &&
        ((NewsItemHTML) otherItem).getLink().equals(getLink());
  }
}
