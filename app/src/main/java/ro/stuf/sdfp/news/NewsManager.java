package ro.stuf.sdfp.news;

import java.util.List;

public class NewsManager {
  public static interface Callback {
    public void onItemsReceived(List<NewsItem> items);
  }

  private HTTPRequest request;
  private List<NewsItem> currentItems;
  private boolean isItemListChanged = true;

  public void clear() {
    currentItems = null;
  }

  public void getItems(final Callback callback) {
    if (request != null) {
      return;
    }

    if (currentItems != null) {
      if (callback != null) {
        callback.onItemsReceived(currentItems);
      }
      return;
    }

    request = new HTTPRequest(new HTTPRequest.Callback() {
      @Override
      public void onSuccess(String result) {
        setCurrentItems(new NewsItemParser().parse(result));
        done();
        if (callback != null) {
          callback.onItemsReceived(currentItems);
        }
      }

      @Override
      public void onError(Exception exception) {
        done();
      }

      private void done() {
        request = null;
      }
    });
    request.execute("http://slashdot.org");
  }

  public List<NewsItem> getCurrentItems() {
    return currentItems;
  }

  private void setCurrentItems(List<NewsItem> currentItems) {
    if (this.currentItems == null || this.currentItems.size() != currentItems.size()) {
      isItemListChanged = true;
    } else {
      isItemListChanged = false;
      for (int i = 0, len = currentItems.size(); i < len; ++i) {
        if (!currentItems.get(i).equals(this.currentItems.get(i))) {
          isItemListChanged = true;
          break;
        }
      }
    }
    this.currentItems = currentItems;
  }

  public boolean isItemListChanged() {
    return isItemListChanged;
  }

  public void setItemListChanged(boolean isItemListChanged) {
    this.isItemListChanged = isItemListChanged;
  }
}
