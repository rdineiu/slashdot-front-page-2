package ro.stuf.sdfp.themes;

public class Theme {
  public final int id;
  public final int resourceId;
  public final String title;

  public Theme(final int id, final int resourceId, final String title) {
    this.id = id;
    this.resourceId = resourceId;
    this.title = title;
  }

  @Override
  public boolean equals(Object otherTheme) {
    return otherTheme instanceof Theme && id == ((Theme) otherTheme).id;
  }
}
