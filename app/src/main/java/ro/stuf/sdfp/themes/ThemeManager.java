package ro.stuf.sdfp.themes;

import android.content.Context;
import android.content.SharedPreferences;
import ro.stuf.sdfp.App;
import ro.stuf.sdfp.R;

public class ThemeManager {
  private static final Theme[] themes = {
      new Theme(1, R.style.App_Theme_Dark, "Dark"),
      new Theme(2, R.style.App_Theme_Light, "Light"),
      new Theme(3, R.style.App_Theme_Teal_Dark, "Teal"),
      new Theme(4, R.style.App_Theme_Purple_Light, "Purple"),
      new Theme(5, R.style.App_Theme_Indigo_Light, "Indigo"),
      new Theme(6, R.style.App_Theme_Steel_Dark, "Steel"),
      new Theme(7, R.style.App_Theme_Black_Dark, "Black"),
      new Theme(8, R.style.App_Theme_White_Light, "White"),
      new Theme(9, R.style.App_Theme_Brick_Dark, "Red")
  };
  private static final int defaultThemeId = 1;

  private Theme currentTheme = null;
  private boolean isCurrentThemeChanged = false;

  public Theme[] getThemes() {
    return themes;
  }

  public Theme getCurrentTheme() {
    if (currentTheme == null) {
      final SharedPreferences preferences = App.context.getSharedPreferences("theme", Context.MODE_PRIVATE);
      final Theme theme = findById(preferences.getInt("themeId", defaultThemeId));
      currentTheme = theme == null ? findById(defaultThemeId) : theme;
    }
    return currentTheme;
  }

  public void setCurrentTheme(final Theme theme) {
    isCurrentThemeChanged = !currentTheme.equals(theme);
    if (isCurrentThemeChanged) {
      currentTheme = theme;
      final SharedPreferences preferences = App.context.getSharedPreferences("theme", Context.MODE_PRIVATE);
      SharedPreferences.Editor editor = preferences.edit();
      editor.putInt("themeId", currentTheme.id);
      editor.apply();
    }
  }

  public boolean isCurrentThemeChanged() {
    boolean isChanged = isCurrentThemeChanged;
    isCurrentThemeChanged = false;
    return isChanged;
  }

  private Theme findById(final int id) {
    for (Theme theme : getThemes()) {
      if (theme.id == id) {
        return theme;
      }
    }
    return null;
  }
}
